#/bin/bash

set -x
set -e
TH_GIT_PATH="$1"
TH_JOB_WORKING_DIR="$2"
TH_UNIQUE_ID="$3"
cd "$TH_JOB_WORKING_DIR"
JOBS=$(grep -c '^processor' /proc/cpuinfo)

if [[ $TH_JOB_NAME =~ .*-qt55-.* ]]; then
    TARGET_NAME=el7-qt55-x86_64
elif [[ $TH_JOB_NAME =~ .*-qt56-.* ]]; then
    TARGET_NAME=el7-qt56-x86_64
elif [[ $TH_JOB_NAME =~ .*-qt57-.* ]]; then
    TARGET_NAME=el7-qt57-x86_64
elif [[ $TH_JOB_NAME =~ .*-qt58-.* ]]; then
    TARGET_NAME=el7-qt58-x86_64
else
    echo "Unrecognized Qt5 branch"
    exit 1
fi

if [[ $TH_JOB_NAME =~ .*-asan-.* ]]; then
    TARGET_NAME=${TARGET_NAME}-asan
fi

PREFIX=/home/turbo-hipster/target/${TARGET_NAME}/shared/general/cmake
mkdir -p ${PREFIX}

time git clone git://cmake.org/cmake.git
cd cmake
time ./bootstrap --prefix=${PREFIX}
time make -j${JOBS}
time make -j${JOBS} install
