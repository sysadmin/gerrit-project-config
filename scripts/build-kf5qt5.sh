#/bin/bash

set -x
set -e
export TH_GIT_PATH="$1"
export TH_JOB_WORKING_DIR="$2"
export TH_UNIQUE_ID="$3"
export XDG_RUNTIME_DIR=`mktemp -d`
env
df -h
free
hostname

cd ~/scripts
git pull
pushd dependencies
git pull
popd
if [[ $TH_JOB_NAME =~ .*-nox11-.* ]]; then
    export VARIATION="NoX11"
elif [[ $TH_JOB_NAME =~ .*-release-minimal-.* ]]; then
    export VARIATION="release-minimal"
else
    unset VARIATION
fi
if [[ $TH_JOB_NAME =~ .*-qt52-.* ]]; then
    QT_VERSION=52
elif [[ $TH_JOB_NAME =~ .*-qt54-.* ]]; then
    QT_VERSION=54
elif [[ $TH_JOB_NAME =~ .*-qt55-.* ]]; then
    QT_VERSION=55
elif [[ $TH_JOB_NAME =~ .*-qt56-.* ]]; then
    QT_VERSION=56
elif [[ $TH_JOB_NAME =~ .*-qt57-.* ]]; then
    QT_VERSION=57
elif [[ $TH_JOB_NAME =~ .*-qt58-.* ]]; then
    QT_VERSION=58
fi

if [[ $TH_JOB_NAME =~ .*-clang-.* ]]; then
    export CC=/opt/llvm-3.8.0/bin/clang
    export CXX=/opt/llvm-3.8.0/bin/clang++
else
    export CC=gcc
    export CXX=g++
fi

if [[ $TH_JOB_NAME =~ .*-asan-.* ]]; then
    export CXXFLAGS="-fsanitize=address -fsanitize=undefined -fno-omit-frame-pointer"
    export LDFLAGS="-fsanitize=address -fsanitize=undefined"
    CI_PLATFORM_SUFFIX=-asan
fi

# The compiler is deliberately not included in here because I'm targeting Linux now, and both are fortunately ABI compatible
if [[ ! -z $QT_VERSION ]]; then
    CI_PLATFORM=th-rhel7-qt${QT_VERSION}
else
    CI_PLATFORM=th-rhel7-gcc
fi
CI_PLATFORM=${CI_PLATFORM}${CI_PLATFORM_SUFFIX}

python2.7 -u ~/scripts/tools/perform-build.py --project $ZUUL_PROJECT --branchGroup kf5-qt5 --sources $TH_GIT_PATH --platform $CI_PLATFORM --variation ${VARIATION:-None}
du -chs ~/target/*
