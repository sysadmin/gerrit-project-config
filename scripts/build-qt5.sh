#/bin/bash

set -x
set -e
TH_GIT_PATH="$1"
TH_JOB_WORKING_DIR="$2"
TH_UNIQUE_ID="$3"
cd "$TH_JOB_WORKING_DIR"

JOBS=$(grep -c '^processor' /proc/cpuinfo)

if [[ $TH_JOB_NAME =~ .*-qt52-.* ]]; then
    QT5_BRANCH=5.2
    QT5_TAG=v5.2.1 # because there's no branch anymore
    TARGET_NAME=el7-qt52-x86_64
elif [[ $TH_JOB_NAME =~ .*-qt54-.* ]]; then
    QT5_BRANCH=5.4
    TARGET_NAME=el7-qt54-x86_64
elif [[ $TH_JOB_NAME =~ .*-qt55-.* ]]; then
    QT5_BRANCH=5.5
    #QT5_TAG=v5.5.1
    TARGET_NAME=el7-qt55-x86_64
elif [[ $TH_JOB_NAME =~ .*-qt56-.* ]]; then
    QT5_BRANCH=5.6
    TARGET_NAME=el7-qt56-x86_64
elif [[ $TH_JOB_NAME =~ .*-qt57-.* ]]; then
    QT5_BRANCH=5.7
    TARGET_NAME=el7-qt57-x86_64
    EXTRA_QT_MODULES=",qtwebkit"
elif [[ $TH_JOB_NAME =~ .*-qt58-.* ]]; then
    QT5_BRANCH=5.8
    TARGET_NAME=el7-qt58-x86_64
    # https://bugreports.qt.io/browse/QTBUG-55950
    EXTRA_QT_MODULES=",-qtwebkit"
else
    echo "Unrecognized Qt5 branch"
    exit 1
fi

if [[ $TH_JOB_NAME =~ .*-release-.* ]]; then
    DEBUG_RELEASE=-release
else
    DEBUG_RELEASE=-debug
fi

if [[ $TH_JOB_NAME =~ .*-clang-.* ]]; then
    export PATH=/opt/llvm-3.8.0/bin:${PATH}
    KDE_MKSPEC="--platform=linux-clang"
    if [[ !($TH_JOB_NAME =~ .*-qt55-.*) ]]; then
        # std::gets got removed in C++14, but GCC 4.8's includes are not ready for that.
        # This only affects clang in our setup because Qt won't enable C++14/C++1z on our version of GCC.
        # Oh, but qtbase's configure didn't have that operator prior to 5.6
        KDE_MKSPEC="${KDE_MKSPEC} --c++std=c++11"
    fi
else
    unset KDE_MKSPEC
fi

if [[ $TH_JOB_NAME =~ .*-asan-.* ]]; then
    SANITIZERS="--sanitize=address --sanitize=undefined"
    TARGET_NAME="${TARGET_NAME}-asan"
    EXTRA_QT_MODULES="${EXTRA_QT_MODULES},-qtwebengine"
else
    unset SANITIZERS
fi

PREFIX=/home/turbo-hipster/target/${TARGET_NAME}/kf5-qt5/qt5/inst
mkdir -p ${PREFIX}

time git clone https://code.qt.io/qt/qt5.git
cd qt5
time git checkout ${QT5_TAG:-${QT5_BRANCH}}
time ./init-repository -f --module-subset=default${EXTRA_QT_MODULES}
time ./configure ${DEBUG_RELEASE} -confirm-license -opensource -dbus -xcb -nomake examples -nomake tests -prefix ${PREFIX} ${KDE_MKSPEC} ${SANITIZERS}
time make -j$JOBS
time make -j$JOBS install

if [[ $TH_JOB_NAME =~ .*-asan-.* ]]; then
    time git clone https://github.com/jktjkt/qtwebengine.git
    pushd qtwebengine
    git checkout linux-asan
    sed -i 's|../qtwebengine-chromium.git|git://code.qt.io/qt/qtwebengine-chromium.git|' .gitmodules
    git submodule update --init
    ${PREFIX}/bin/qmake \
        CONFIG+=release CONFIG-=sanitize_undefined \
        INCLUDEPATH+=/opt/llvm-3.8.0/include -spec linux-clang
    time make -j$JOBS
    time make -j$JOBS install
    popd
fi

if [[ $TH_JOB_NAME =~ .*-qt58-.* ]]; then
    time git clone git://code.qt.io/qt/qtwebkit.git
    pushd qtwebkit
    git fetch https://codereview.qt-project.org/qt/qtwebkit refs/changes/13/171013/1
    git checkout FETCH_HEAD
    ${PREFIX}/bin/qmake CONFIG-=sanitize_undefined
    time make -j$JOBS
    time make -j$JOBS install
    popd
fi
