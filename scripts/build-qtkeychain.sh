#/bin/bash

set -x
set -e

mkdir work-build-qtkeychain
cd work-build-qtkeychain
mkdir work
time git clone https://github.com/frankosterfeld/qtkeychain.git
export ZUUL_PROJECT=qtkeychain
/opt/project-config/scripts/build-kf5qt5.sh `pwd`/qtkeychain `pwd`/work 666
cd ..
rm -rf work-build-qtkeychain
