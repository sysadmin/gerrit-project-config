#/bin/bash

set -x
set -e
TH_GIT_PATH="$1"
TH_JOB_WORKING_DIR="$2"
TH_UNIQUE_ID="$3"
cd "$TH_JOB_WORKING_DIR"

if [[ $TH_JOB_NAME =~ .*-debug-.* ]]; then
    WITH_DEBUG="-DCMAKE_BUILD_TYPE=Debug"
elif [[ $TH_JOB_NAME =~ .*-release-.* ]]; then
    WITH_DEBUG="-DCMAKE_BUILD_TYPE=RelWithDebInfo"
elif [[ $TH_JOB_NAME =~ .*-mingw.* ]]; then
    WITH_DEBUG="-DCMAKE_BUILD_TYPE=Release"
fi

if [[ $TH_JOB_NAME =~ .*-clang-.* ]]; then
    export CXX=clang++
    export CC=clang
fi

if [[ $TH_JOB_NAME =~ .*-minimal-.* ]]; then
    PACKAGE_OPTIONS="-DCMAKE_DISABLE_FIND_PACKAGE_ZLIB=true -DCMAKE_DISABLE_FIND_PACKAGE_RagelForTrojita=true -DCMAKE_DISABLE_FIND_PACKAGE_Qt5DBus=true"
elif [[ $TH_JOB_NAME =~ .*-mingw32-.* ]]; then
    PACKAGE_OPTIONS="-DCMAKE_SKIP_RPATH:BOOL=ON -DCMAKE_TOOLCHAIN_FILE=/usr/share/mingw/toolchain-mingw32.cmake -DWITH_TESTS=OFF -DBUILD_TESTING=OFF -DWITH_NSIS=ON -DOpenSSL_SSL_LOC:PATH=/usr/i686-w64-mingw32/sys-root/mingw/bin/libssl-10.dll -DOpenSSL_Crypto_LOC:PATH=/usr/i686-w64-mingw32/sys-root/mingw/bin/libcrypto-10.dll"
else
    unset PACKAGE_OPTIONS
fi

if [[ $TH_JOB_NAME =~ .*-mingw32-.* ]]; then
  CMAKE=~/preseed/el7-qt56-x86_64/shared/general/cmake/bin/cmake
  unset CTEST
else
  CMAKE=cmake
  CTEST=ctest
fi

pushd $TH_GIT_PATH
python l10n-fetch-po-files.py
popd

mkdir build
cd build
$CMAKE $WITH_DEBUG $PACKAGE_OPTIONS "$TH_GIT_PATH"
make -j16 VERBOSE=1
if [[ -n "${CTEST}" ]]; then
    xvfb-run $CTEST -j 666 --output-on-failure
else
    echo "Tests are skipped in this build profile"
fi

if [[ $TH_JOB_NAME =~ .*-mingw32-.* ]]; then
    # Upload the resulting binary. Yes, this is a hack.
    REMOTE_SERVER=th-ci-logs@ci-logs.kde.flaska.net
    REMOTE_PATH=public_html/binaries/trojita/win32/${ZUUL_PIPELINE}/
    ssh ${REMOTE_SERVER} mkdir -p ${REMOTE_PATH}
    if [[ $ZUUL_PIPELINE == 'check' ]]; then
        scp Trojita-Setup.exe ${REMOTE_SERVER}:${REMOTE_PATH}/Trojita-Setup-${ZUUL_CHANGE}-${ZUUL_UUID}.exe
    else
        scp Trojita-Setup.exe ${REMOTE_SERVER}:${REMOTE_PATH}/Trojita-Setup-$(GIT_DIR=$TH_GIT_PATH/.git git describe).exe
    fi
fi
