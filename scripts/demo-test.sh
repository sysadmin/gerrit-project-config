#!/bin/bash

set -x

TH_GIT_PATH="$1"
TH_JOB_WORKING_DIR="$2"
TH_UNIQUE_ID="$3"

env
uptime
free
df -h

cd "$TH_JOB_WORKING_DIR"
ls -al
sleep 20
