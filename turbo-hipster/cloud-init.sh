#!/bin/bash

exec > /var/log/kde-boot.log 2>&1
set -x
set -e

if [ ! -d /root/.ssh ] ; then
  mkdir -p /root/.ssh
  chmod 0700 /root/.ssh
  curl http://vorvan.flaska.net/public_key_jkt > /root/.ssh/authorzed_keys
  chmod 0600 /root/.ssh/authorzed_keys
  restorecon -r /root/.ssh
fi

if cat /etc/redhat-release | grep -q 'release 7' ; then
  OSVERSION=el7
else
  echo "Unrecognized OS version"
  exit 1
fi

yum update -y
yum install -y epel-release

if [[ $OSVERSION == el7 ]]; then
  curl http://download.opensuse.org/repositories/home:/jkt-gentoo:/kde-ci/CentOS_7/home:jkt-gentoo:kde-ci.repo > /etc/yum.repos.d/kde-ci.repo
  curl https://copr.fedorainfracloud.org/coprs/alonid/llvm-3.8.0/repo/epel-7/alonid-llvm-3.8.0-epel-7.repo > /etc/yum.repos.d/alonid-llvm-3.8.0-epel-7.repo
  yum install -y \
	  clang gcc-c++ git xorg-x11-server-Xvfb lsof strace \
	  python-pbr python-devel python-simplejson cmake subversion clang-3.8.0 compiler-rt-3.8.0 \
          perl-version libxcb-devel xcb-util-devel xcb-util-*-devel libX11-devel libXrender-devel libXi-devel perl \
          python ruby flex bison gperf libicu-devel libxslt-devel libgcrypt-devel pciutils-devel nss-devel libXtst-devel \
          cups-devel pulseaudio-libs-devel libgudev1-devel systemd-devel libcap-devel alsa-lib-devel gstreamer-plugins-base-devel \
          dbus-devel libXcomposite-devel \
          libsrtp-devel snappy-devel mesa-libGL-devel mesa-libEGL-devel zlib-devel libwebp-devel libevent-devel jsoncpp-devel opus-devel libXScrnSaver-devel \
          sqlite-devel perl-Digest-MD5 \
          python-lxml openbox dbus-x11 mesa-dri-drivers docbook-style-xsl \
          {glib2,polkit,xcb-util-keysyms,libxslt,systemd,libattr,libacl,boost,pcre,libgit2,libepoxy}-devel \
          perl-URI dejavu-{sans,serif,sans-mono}-fonts lmdb-devel giflib-devel libjpeg-turbo-devel libpng-devel \
          NetworkManager-devel NetworkManager-libnm-devel ModemManager-devel bzip2-devel libSM-devel \
          gpgme-devel hicolor-icon-theme \
          mingw32-openssl mingw32-qt5-qtwebkit mingw32-qt5-qttools mingw32-qt5-qmldevtools-devel mingw32-qt5-qtsvg \
          mimetic-devel fdupes
  # we need something newer than stuff in EPEL, and OBS would require a ton of EPEL duplication...
  yum localinstall -y http://vorvan.flaska.net/mingw32-nsis-3.0b1-1.el7.centos.x86_64.rpm
fi

curl https://bootstrap.pypa.io/get-pip.py | python2.7

cd /opt
git clone https://gerrit.vesnicky.cesnet.cz/r/p/sysadmin/gerrit-gear gear
cd gear
git checkout kde
pip install .
cd ..
git clone https://gerrit.vesnicky.cesnet.cz/r/p/sysadmin/gerrit-turbo-hipster turbo-hipster
cd turbo-hipster
git checkout kde
pip install .
cd ..
git clone https://gerrit.vesnicky.cesnet.cz/r/p/sysadmin/gerrit-project-config project-config
cd /root

useradd turbo-hipster
mkdir ~turbo-hipster/{logs,jobs,git,.ssh}
chown turbo-hipster:turbo-hipster ~turbo-hipster/{logs,jobs,git,.ssh}
cat /opt/project-config/turbo-hipster/config.yaml /opt/project-config/turbo-hipster/config-$OSVERSION.yaml > ~turbo-hipster/config.yaml
curl http://vorvan.flaska.net/id_rsa-th-logs > ~turbo-hipster/.ssh/id_rsa
curl http://vorvan.flaska.net/id_rsa-th-logs.pub > ~turbo-hipster/.ssh/id_rsa.pub
chown turbo-hipster:turbo-hipster ~turbo-hipster/.ssh/id_rsa*
chmod 0600 ~turbo-hipster/.ssh/id_rsa*
su - turbo-hipster -c 'ssh-keyscan ci-logs.kde.flaska.net > ~/.ssh/known_hosts'

cd ~turbo-hipster
git clone https://github.com/jktjkt/th-www-build-kde-org.git scripts
cd scripts
git checkout th-build
mkdir dependencies
cd dependencies
git clone git://anongit.kde.org/kde-build-metadata .
chown -R turbo-hipster:turbo-hipster ~turbo-hipster/scripts
cd /root

if [[ $OSVERSION == el7 ]]; then
  su turbo-hipster -c 'for platform in el7-qt5{2,5,6,7,8}-x86_64 el7-qt58-x86_64-asan; do for project in kf5-qt5/qt5 shared/general/cmake; do mkdir -p ~/preseed/$platform/$project; rsync -aqP th-kde-artefacts@ci-logs.kde.flaska.net:depot/$platform/$project/ ~/preseed/$platform/$project; done; done'
  su turbo-hipster -c 'mkdir ~/.ssh/sock/; chmod 700 ~/.ssh/sock/; echo -e "Host ci-logs.kde.flaska.net\n ControlMaster auto\n ControlPath ~/.ssh/sock/%r@%h-%p\n ControlPersist 5" > ~/.ssh/config'
  cat > /etc/systemd/system/turbo-hipster.service <<EOF
[Unit]
Description=Turbo-Hipster test launcher

[Service]
Type=simple
User=turbo-hipster
Group=turbo-hipster
ExecStart=/usr/bin/turbo-hipster -c /home/turbo-hipster/config.yaml
ExecStopPost=/usr/bin/bash -c 'rm -rf ~turbo-hipster/{git,target,.kde-unit-test,.qttest,.local,.cache,.config,.pki,jobs}; /usr/bin/find /tmp -user turbo-hipster -delete'
Restart=always

[Install]
WantedBy=multi-user.target
EOF
  systemctl enable turbo-hipster.service
fi
reboot
